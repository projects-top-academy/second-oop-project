//
// Created by yurik on 19.03.2024.
//

#include <iostream>
#include <vector>
#include <string>


class Room{
private:
    std::string name;
    double width;
    double length;

public:
    Room(std::string n, double w, double l) : name(n), width(w), length(l) {}

    double getArea(){
        return width * length;
    }

    std::string getName(){
        return name;
    }
};

class WallpaperRoll{
private:
    std::string name;
    double width;
    double length;
    double price;
public:
    WallpaperRoll(std::string n, double w, double l, double p) : name(n), width(w), length(l), price(p) {}

    double getArea() {
        return width * length;
    }

    double getPrice(){
        return price;
    }

    std::string getName(){
        return name;
    }
};

class Apartment{
private:
    std::vector<Room> rooms;
public:
    void addRoom(Room room){
        rooms.push_back(room);
    }

    int getNumRooms(){
        return rooms.size();
    }

    Room getRoom(int index){
        return rooms[index];
    }
};
