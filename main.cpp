#include <iostream>
#include <string>
#include "Classes.h"

/*

 Задача

  Написать программу "Стоимость обоев"
  Программа запрашивает:
  -кол-во комнат в квартире, в которых планируется клеить обои
  -параметры каждой комнаты
  -параметры каждого вида обоев

  В результате работы программы она выдаёт необходимое кол-во рклонов каждого вида для каждой комнаты,
  общую стоимость закупки обоев.

  Реализовать классы:
  1. Квартира (список комнат)
  2. Комната (размер, название)
  3. РулонОбоев (название, размер - длина рулона и ширина, цена за рулон)

 */


int main() {
    Apartment apartment;

    int numRooms;

    std::cout << "Enter the number of rooms int the apartment: ";
    std::cin >> numRooms;

    for (int i = 0; i < numRooms; ++i) {
        std::string roomName;
        double width, lenght;

        std::cout << "Enter the name of room " << i + 1 << ": ";
        std::cin >> roomName;

        std::cout << "Enter the width of the room: ";
        std::cin >> width;

        std::cout << "Enter the lenght of the room: ";
        std::cin >> lenght;

        Room room(roomName, width, lenght);
        apartment.addRoom(room);
    }

    WallpaperRoll wallpaper("Simple Wallpaper", 1.5, 15.5, 34.0);

    double  totalCost = 0.0;

    for (int i = 0; i < apartment.getNumRooms(); ++i) {
        Room room = apartment.getRoom(i);
        double roomArea = room.getArea();
        int rollsNeeded = static_cast<int>(roomArea/ wallpaper.getArea()) + 1;

        std::cout << "For room " << room.getName() << ", you need " << rollsNeeded << " rolls of " << wallpaper.getName() << " wallpaper.\n";

        totalCost += rollsNeeded * wallpaper.getPrice();
    }

    std::cout << "Total cost of wallpaper: $" << totalCost << "\n";

    return 0;
}
